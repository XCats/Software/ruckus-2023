package frc.team191;

// import com.pathplanner.lib.PathConstraints;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import java.util.List;

import com.revrobotics.CANSparkBase.IdleMode;
public final class Constants {

    /* Field related constants */
    public static final class FieldConstants {
      // List of possible scoring locations as Pose2d objects
      public static final List<Pose2d> SCORING_POSITIONS =
          List.of(
              new Pose2d(
                  new Translation2d(0.555, 7.436),
                  Rotation2d.fromRadians(Math.PI)), // Red loading double station
              new Pose2d(new Translation2d(0.555, 6.146), Rotation2d.fromRadians(Math.PI)),
              new Pose2d(
                  new Translation2d(15.03, 5.061),
                  Rotation2d.fromDegrees(0.0)), // Red node scoring locations
              new Pose2d(new Translation2d(15.03, 4.405), Rotation2d.fromDegrees(0.0)),
              new Pose2d(new Translation2d(15.03, 3.846), Rotation2d.fromDegrees(0.0)),
              new Pose2d(new Translation2d(15.03, 3.298), Rotation2d.fromDegrees(0.0)),
              new Pose2d(new Translation2d(15.03, 2.74), Rotation2d.fromDegrees(0.0)),
              new Pose2d(new Translation2d(15.03, 2.2), Rotation2d.fromDegrees(0.0)),
              new Pose2d(new Translation2d(15.03, 1.62), Rotation2d.fromDegrees(0.0)),
              new Pose2d(new Translation2d(15.03, 1.06), Rotation2d.fromDegrees(0.0)),
              new Pose2d(new Translation2d(15.03, 0.52), Rotation2d.fromDegrees(0.0)),
              new Pose2d(
                  new Translation2d(15.64, 7.430),
                  Rotation2d.fromDegrees(0.0)), // Blue loading double substation
              new Pose2d(new Translation2d(15.64, 6.16), Rotation2d.fromDegrees(0.0)),
              new Pose2d(
                  new Translation2d(1.598, 4.996),
                  Rotation2d.fromRadians(-Math.PI)), // Blue node scoring locations
              new Pose2d(new Translation2d(1.598, 4.373), Rotation2d.fromRadians(-Math.PI)),
              new Pose2d(new Translation2d(1.598, 3.85), Rotation2d.fromRadians(-Math.PI)),
              new Pose2d(new Translation2d(1.598, 3.3), Rotation2d.fromRadians(-Math.PI)),
              new Pose2d(new Translation2d(1.598, 2.75), Rotation2d.fromRadians(-Math.PI)),
              new Pose2d(new Translation2d(1.598, 2.2), Rotation2d.fromRadians(-Math.PI)),
              new Pose2d(new Translation2d(1.598, 1.63), Rotation2d.fromRadians(-Math.PI)),
              new Pose2d(new Translation2d(1.598, 1.05), Rotation2d.fromRadians(-Math.PI)),
              new Pose2d(new Translation2d(1.598, 0.5), Rotation2d.fromRadians(-Math.PI)));
    }
  
    /** General robot constants */
    public static final class GeneralConstants {

      // Driver controller port
      public static final int DRIVER_CONTROLLER_ID = 0;

      // Operator controller port
      public static final int OPERATOR_CONTROLLER_ID = 1;

      public static final boolean IS_TUNING_MODE = false;

    }

    public static class IntakeConstants {

      public static final int INTAKE_MOTOR_TOP = 8;
      public static final int INTAKE_MOTOR_BOT = 9;
      public static final int BEAM_BREAK_DIO = 0;

      public static final int CURRENT_LIMIT_INTAKE = 40;

      public static final double INTAKE_PID0_P = 0.001;
      public static final double INTAKE_PID0_I = 0;
      public static final double INTAKE_PID0_D = 0;
      public static final double INTAKE_PID0_F = 0.0001;

      public static final double OUTPUT_RANGE_MIN = -1;
      public static final double OUTPUT_RANGE_MAX = 1;

      public static final double INTAKE_PID1_P = 0;
      public static final double INTAKE_PID1_I = 0;
      public static final double INTAKE_PID1_D = 0;
      public static final double INTAKE_PID1_F = 0;

      public static enum Intake_Speed {
        CUBE_INTAKE_SPEED,
        SHOOT_HIGH_CUBE,
        SHOOT_MAX_SPEED,
        SHOOT_MID_CUBE,
        SHOOT_LOW_CUBE;
    
        public double getValue() {
          switch (this) {
            case CUBE_INTAKE_SPEED:
              return -1.0;
            case SHOOT_MAX_SPEED:
              return 1.0;
            case SHOOT_HIGH_CUBE:
              return 1.0;
            case SHOOT_MID_CUBE:
              return 0.5;
            case SHOOT_LOW_CUBE:
              return 0.5;
            default:
              return 0;
          }
        }
      }
    }

    public static class ArmConstants {

      public static final int ARM_MOTOR = 6;
      public static final int ARM_MOTOR_FOLLOWER = 7;

      public static final int CURRENT_LIMIT_ARM = 30;
      public static final double ARM_PID0_P = 0.01;
      public static final double ARM_PID0_I = 0;
      public static final double ARM_PID0_D = 0;
      public static final double ARM_PID0_F = 0;
      public static final double OUTPUT_RANGE_MIN = -1;
      public static final double OUTPUT_RANGE_MAX = 1;

      public static final double INTAKE_POSITION = 260;
      public static final double REST_POSITION = 180;
      public static final double MID_POSITION = 190;
      public static final double LOW_POSITION = 230;

    }
  
    public static final class DriveConstants {
    // Driving Parameters - Note that these are not the maximum capable speeds of
    // the robot, rather the allowed maximum speeds
    public static final double kMaxSpeedMetersPerSecond = 4.8;
    public static final double kMaxAngularSpeed = 2 * Math.PI; // radians per second

    public static final double kDirectionSlewRate = 1.2; // radians per second
    public static final double kMagnitudeSlewRate = 1.8; // percent per second (1 = 100%)
    public static final double kRotationalSlewRate = 2.0; // percent per second (1 = 100%)
    public static  final int kPigeonCanId = 16;
    // Chassis configuration
    public static final double kTrackWidth = Units.inchesToMeters(26.5);
    // Distance between centers of right and left wheels on robot
    public static final double kWheelBase = Units.inchesToMeters(26.5);
    // Distance between front and back wheels on robot
    public static final SwerveDriveKinematics kDriveKinematics = new SwerveDriveKinematics(
        new Translation2d(kWheelBase / 2, kTrackWidth / 2),
        new Translation2d(kWheelBase / 2, -kTrackWidth / 2),
        new Translation2d(-kWheelBase / 2, kTrackWidth / 2),
        new Translation2d(-kWheelBase / 2, -kTrackWidth / 2));

    // Angular offsets of the modules relative to the chassis in radians
    public static final double kFrontLeftChassisAngularOffset = -Math.PI / 2;
    public static final double kFrontRightChassisAngularOffset = 0;
    public static final double kBackLeftChassisAngularOffset = Math.PI;
    public static final double kBackRightChassisAngularOffset = Math.PI / 2;

    // SPARK MAX CAN IDs
    public static final int kFrontLeftDrivingCanId = 12;
    public static final int kRearLeftDrivingCanId = 14;
    public static final int kFrontRightDrivingCanId = 2;
    public static final int kRearRightDrivingCanId = 4;

    public static final int kFrontLeftTurningCanId = 13;
    public static final int kRearLeftTurningCanId = 15;
    public static final int kFrontRightTurningCanId = 3;
    public static final int kRearRightTurningCanId = 5;

    public static final boolean kGyroReversed = false;
  }

  public static final class ModuleConstants {
    // The MAXSwerve module can be configured with one of three pinion gears: 12T, 13T, or 14T.
    // This changes the drive speed of the module (a pinion gear with more teeth will result in a
    // robot that drives faster).
    public static final int kDrivingMotorPinionTeeth = 14;

    // Invert the turning encoder, since the output shaft rotates in the opposite direction of
    // the steering motor in the MAXSwerve Module.
    public static final boolean kTurningEncoderInverted = true;

    // Calculations required for driving motor conversion factors and feed forward
    public static final double kDrivingMotorFreeSpeedRps = NeoMotorConstants.kFreeSpeedRpm / 60;
    public static final double kWheelDiameterMeters = 0.0762;
    public static final double kWheelCircumferenceMeters = kWheelDiameterMeters * Math.PI;
    // 45 teeth on the wheel's bevel gear, 22 teeth on the first-stage spur gear, 15 teeth on the bevel pinion
    public static final double kDrivingMotorReduction = (45.0 * 22) / (kDrivingMotorPinionTeeth * 15);
    public static final double kDriveWheelFreeSpeedRps = (kDrivingMotorFreeSpeedRps * kWheelCircumferenceMeters)
        / kDrivingMotorReduction;

    public static final double kDrivingEncoderPositionFactor = (kWheelDiameterMeters * Math.PI)
        / kDrivingMotorReduction; // meters
    public static final double kDrivingEncoderVelocityFactor = ((kWheelDiameterMeters * Math.PI)
        / kDrivingMotorReduction) / 60.0; // meters per second

    public static final double kTurningEncoderPositionFactor = (2 * Math.PI); // radians
    public static final double kTurningEncoderVelocityFactor = (2 * Math.PI) / 60.0; // radians per second

    public static final double kTurningEncoderPositionPIDMinInput = 0; // radians
    public static final double kTurningEncoderPositionPIDMaxInput = kTurningEncoderPositionFactor; // radians

    public static final double kDrivingP = 0.04;
    public static final double kDrivingI = 0;
    public static final double kDrivingD = 0;
    public static final double kDrivingFF = 1 / kDriveWheelFreeSpeedRps;
    public static final double kDrivingMinOutput = -1;
    public static final double kDrivingMaxOutput = 1;

    public static final double kTurningP = 1;
    public static final double kTurningI = 0;
    public static final double kTurningD = 0;
    public static final double kTurningFF = 0;
    public static final double kTurningMinOutput = -1;
    public static final double kTurningMaxOutput = 1;

    public static final IdleMode kDrivingMotorIdleMode = IdleMode.kBrake;
    public static final IdleMode kTurningMotorIdleMode = IdleMode.kBrake;

    public static final int kDrivingMotorCurrentLimit = 50; // amps
    public static final int kTurningMotorCurrentLimit = 20; // amps
  }

  public static final class OIConstants {
    public static final int kDriverControllerPort = 0;
    public static final double kDriveDeadband = 0.05;
  }

  public static final class AutoConstants {
    public static final double kMaxSpeedMetersPerSecond = 3;
    public static final double kMaxAccelerationMetersPerSecondSquared = 3;
    public static final double kMaxAngularSpeedRadiansPerSecond = Math.PI;
    public static final double kMaxAngularSpeedRadiansPerSecondSquared = Math.PI;

    public static final double kPXController = 1;
    public static final double kPYController = 1;
    public static final double kPThetaController = 1;

    // Constraint for the motion profiled robot angle controller
    public static final TrapezoidProfile.Constraints kThetaControllerConstraints = new TrapezoidProfile.Constraints(
        kMaxAngularSpeedRadiansPerSecond, kMaxAngularSpeedRadiansPerSecondSquared);
  }

  public static final class NeoMotorConstants {
    public static final double kFreeSpeedRpm = 5676;
  }
  
    /** Constants revolving around the vision subsystem. */
    public static final class VisionConstants {
      // Camera name
      public static final String CAMERA_NAME = "OV5647";
  
      // Robot to camera transform
      public static final Transform3d ROBOT_TO_CAM =
          new Transform3d(
              new Translation3d(0.0, Units.inchesToMeters(1.5), Units.inchesToMeters(39.0)),
              new Rotation3d(0.0, 0.0, 0.0));
    }
  
    /** Constants revolving around auton modes. */
    public static final class AutonConstants {
  
      public static final double MAX_VELOCITY = 4.0;
      public static final double MAX_ACCELERATION = 3.0;
      // public static final PathConstraints CONSTRAINTS =
      //     new PathConstraints(AutonConstants.MAX_VELOCITY, AutonConstants.MAX_ACCELERATION);
  
      public static final double XY_CONTROLLER_P = 4;
      public static final double THETA_CONTROLLER_P = 2;
    }

    public static class LEDConstants {
      public static final int LED_PORT = 0;
      public static final int FULL_LENGTH = 60;
      public static final int CHASSIS_LENGTH = 60;
      public static final double STROBE_FAST_DURATION = 0.5;
      public static final double STROBE_SLOW_DURATION = 1;
      public static final double BREATH_DURATION = 1.0;
      public static final double RAINBOW_CYCLE_LENGTH = 25.0;
      public static final double RAINBOW_DURATION = 1.0;
      public static final double WAVE_EXPONENT = 0.4;
      public static final double WAVE_FAST_CYCLE_LENGTH = 25.0;
      public static final double WAVE_FAST_DURATION = 0.25;
      public static final double WAVE_SLOW_CYCLE_LENGTH = 25.0;
      public static final double WAVE_SLOW_DURATION = 3.0;
      public static final double WAVE_ALLIANCE_CYCLE_LENGTH = 15.0;
      public static final double WAVE_ALLIANCE_DURATION = 2.0;
      public static final double AUTO_FADE_TIME = 2.5; // 3s nominal
      public static final double AUTO_FADE_MAX_TIME = 5.0; // Return to normal

    }

  }
  
