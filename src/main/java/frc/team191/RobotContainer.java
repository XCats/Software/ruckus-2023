// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.team191;

import java.io.File;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2 .command.button.CommandXboxController;
import frc.team191.Constants.OIConstants;
import frc.team191.commands.IntakeCube;
import frc.team191.commands.IntakeCubeRest;
// import frc.team191.commands.drive.DriveFieldRelative;
// import frc.team191.commands.drive.DriveRobotRelative;
import frc.team191.commands.intake.IntakeSpin;
import frc.team191.commands.intake.OuttakeCube;
import frc.team191.commands.intake.PrepareOuttakeSpeed;
import frc.team191.commands.leds.SetLEDPattern;
import frc.team191.commands.arm.MoveArmToPosition;
import frc.team191.commands.arm.HoldArmPosition;
import frc.team191.subsystems.ArmSubsystem;
import frc.team191.subsystems.IntakeSubsystem;
import frc.team191.subsystems.LEDSubsystem;
import frc.team191.subsystems.DriveSubsystem;
public class RobotContainer 
{

  private final int translationAxis = XboxController.Axis.kLeftY.value;
  private final int strafeAxis = XboxController.Axis.kLeftX.value;
  private final int rotationAxis = XboxController.Axis.kRightX.value;

  CommandXboxController driver = new CommandXboxController(Constants.GeneralConstants.DRIVER_CONTROLLER_ID);
  CommandXboxController operator = new CommandXboxController(Constants.GeneralConstants.OPERATOR_CONTROLLER_ID);

  public final DriveSubsystem drive = new DriveSubsystem();
  public static final IntakeSubsystem intake = new IntakeSubsystem();
  public static final ArmSubsystem    arm    = new ArmSubsystem();
  public static final LEDSubsystem    leds   = new LEDSubsystem();

  public RobotContainer() 
  {
    configureBindings();
     drive.setDefaultCommand(
        // The left stick controls translation of the robot.
        // Turning is controlled by the X axis of the right stick.
        new RunCommand(
            () -> drive.drive(
                -MathUtil.applyDeadband(driver.getLeftY(), OIConstants.kDriveDeadband),
                -MathUtil.applyDeadband(driver.getLeftX(), OIConstants.kDriveDeadband),
                -MathUtil.applyDeadband(driver.getRightX(), OIConstants.kDriveDeadband),
                true, true),
            drive));
    leds.setDefaultCommand(new SetLEDPattern(intake, leds).ignoringDisable(true));
    // intake.setDefaultCommand(new IntakeSpin(intake, 0));  //FIXME maybe?
    arm.setDefaultCommand(new HoldArmPosition(arm)); //FIXME
  }

  private void configureBindings() 
  {
    //Driver Bindings

    //drive Commands
    driver.b().onTrue(new InstantCommand(() -> drive.zeroHeading()));

    // driver.b().onTrue(new InstantCommand(() -> drive.lock()));
    // driver.povUp().onTrue(new InstantCommand(() -> drive.setHeadingAngle(Math.round(drive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2)));
    // driver.povDown().onTrue(new InstantCommand(() -> drive.setHeadingAngle(Math.round(drive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 - Math.PI)));
    // driver.povLeft().onTrue(new InstantCommand(() -> drive.setHeadingAngle(Math.round(drive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 - Math.PI/2)));
    // driver.povRight().onTrue(new InstantCommand(() -> drive.setHeadingAngle(Math.round(drive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 + Math.PI/2)));
    // driver.leftBumper().onTrue(new InstantCommand(() -> drive.setTranslationalScalar(Constants.driveConstants.drive_SLOW_TRANSLATION))).onFalse(new InstantCommand(() -> drive.setTranslationalScalar(Constants.driveConstants.drive_NORMAL_TRANSLATION))); //FIXME

    //Delivery Commands
    driver.leftTrigger().whileTrue(new IntakeCube(intake, arm, Constants.ArmConstants.INTAKE_POSITION)).onFalse(new ParallelCommandGroup( new IntakeSpin(intake, 0), new MoveArmToPosition(arm, Constants.ArmConstants.REST_POSITION))); //FIXME
    driver.rightTrigger().onTrue(new OuttakeCube(intake)).onFalse(new ParallelCommandGroup( new IntakeSpin(intake, 0), new MoveArmToPosition(arm, Constants.ArmConstants.REST_POSITION))); //FIXME


    //Operator Bindings

    //Intake Commands
    operator.rightBumper().onTrue(new PrepareOuttakeSpeed(intake, Constants.IntakeConstants.Intake_Speed.SHOOT_MAX_SPEED));
    operator.y().onTrue(new PrepareOuttakeSpeed(intake, Constants.IntakeConstants.Intake_Speed.SHOOT_HIGH_CUBE));
    operator.b().onTrue(new PrepareOuttakeSpeed(intake, Constants.IntakeConstants.Intake_Speed.SHOOT_MID_CUBE));
    operator.a().onTrue(new PrepareOuttakeSpeed(intake, Constants.IntakeConstants.Intake_Speed.SHOOT_LOW_CUBE));


    //Arm Commands
    operator.povUp().onTrue(new MoveArmToPosition(arm, Constants.ArmConstants.REST_POSITION));
    operator.povRight().onTrue(new MoveArmToPosition(arm, Constants.ArmConstants.MID_POSITION));
    operator.povDown().onTrue(new MoveArmToPosition(arm, Constants.ArmConstants.LOW_POSITION));

    //Delivery Commands
    driver.leftBumper().whileTrue(new IntakeCube(intake, arm, Constants.ArmConstants.MID_POSITION)).onFalse(new ParallelCommandGroup( new IntakeSpin(intake, 0), new MoveArmToPosition(arm, Constants.ArmConstants.REST_POSITION))); //FIXME
  }

  private void configureDemoBindings()
  {
    operator.x().onTrue(new InstantCommand(() -> drive.zeroHeading()));
    driver.b().onTrue(new InstantCommand(() -> drive.setX()));
    // driver.povUp().onTrue(new InstantCommand(() -> drive.setHeadingAngle(Math.round(drive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2)));
    // driver.povDown().onTrue(new InstantCommand(() -> drive.setHeadingAngle(Math.round(drive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 - Math.PI)));
    // driver.povLeft().onTrue(new InstantCommand(() -> drive.setHeadingAngle(Math.round(drive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 - Math.PI/2)));
    // driver.povRight().onTrue(new InstantCommand(() -> drive.setHeadingAngle(Math.round(drive.getYaw().getRadians() / (2.0*Math.PI)) * Math.PI * 2 + Math.PI/2)));
    // operator.rightBumper().onTrue(new InstantCommand(() -> drive.setTranslationalScalar(Constants.driveConstants.drive_SLOW_TRANSLATION))).onFalse(new InstantCommand(() -> drive.setTranslationalScalar(Constants.driveConstants.drive_NORMAL_TRANSLATION)));
    operator.leftBumper().whileTrue(new IntakeCubeRest(intake, arm)).onFalse(new ParallelCommandGroup( new IntakeSpin(intake, 0), new MoveArmToPosition(arm, Constants.ArmConstants.REST_POSITION)));
    operator.rightTrigger().onTrue(new OuttakeCube(intake)).onFalse(new ParallelCommandGroup( new IntakeSpin(intake, 0), new MoveArmToPosition(arm, Constants.ArmConstants.REST_POSITION)));

    operator.y().onTrue(new PrepareOuttakeSpeed(intake, Constants.IntakeConstants.Intake_Speed.SHOOT_MAX_SPEED));
    operator.b().onTrue(new PrepareOuttakeSpeed(intake, Constants.IntakeConstants.Intake_Speed.SHOOT_HIGH_CUBE));
    operator.a().onTrue(new PrepareOuttakeSpeed(intake, Constants.IntakeConstants.Intake_Speed.SHOOT_LOW_CUBE));
    operator.povUp().onTrue(new MoveArmToPosition(arm, Constants.ArmConstants.REST_POSITION));
    operator.povRight().onTrue(new MoveArmToPosition(arm, Constants.ArmConstants.MID_POSITION));
    operator.povDown().onTrue(new MoveArmToPosition(arm, Constants.ArmConstants.LOW_POSITION));
  }
}
 