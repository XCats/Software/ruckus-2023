package frc.team191.commands;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.team191.Constants;
import frc.team191.commands.arm.MoveArmToPosition;
import frc.team191.commands.intake.IntakeSpin;
import frc.team191.commands.intake.WaitForIntake;
import frc.team191.subsystems.ArmSubsystem;
import frc.team191.subsystems.IntakeSubsystem;

public class IntakeCubeRest extends SequentialCommandGroup{
    IntakeSubsystem intake;
    ArmSubsystem arm;

    public IntakeCubeRest(IntakeSubsystem intakeSubsystem, ArmSubsystem armSubsystem)
    {
        intake = intakeSubsystem;
        arm = armSubsystem;
        System.out.println("SOMETHING MYSTERIOUS");
        addCommands
        (
            new ParallelCommandGroup
            (
                new IntakeSpin(intakeSubsystem, Constants.IntakeConstants.Intake_Speed.CUBE_INTAKE_SPEED.getValue()),
                // new MoveArmToPosition(armSubsystem, Constants.ArmConstants.INTAKE_POSITION),
                new WaitForIntake(intakeSubsystem)
            ),
            new IntakeSpin(intake, 0)
            //  new MoveArmToPosition(arm, Constants.ArmConstants.REST_POSITION)

        );
    }

    public void end()
    {
       
    }
}

