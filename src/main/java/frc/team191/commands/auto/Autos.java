package frc.team191.commands.auto;

// import com.pathplanner.lib.PathConstraints;
// import com.pathplanner.lib.PathPlanner;
// import com.pathplanner.lib.auto.PIDConstants;
// import com.pathplanner.lib.auto.SwerveAutoBuilder;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.team191.Constants;
import frc.team191.RobotContainer;
import frc.team191.commands.IntakeCube;
import frc.team191.commands.arm.MoveArmToPosition;
// import frc.team191.commands.drive.Balance;
import frc.team191.commands.intake.IntakeSpin;
import frc.team191.commands.intake.OuttakeCube;
import frc.team191.commands.intake.PrepareOuttakeSpeed;

import java.util.HashMap;

public final class Autos {

  private static final SendableChooser<Command> autonChooser = new SendableChooser<Command>();;
  private static HashMap<String, Command> eventMap;
  // private static SwerveAutoBuilder autonBuilder;

  public static void init() {

    eventMap = new HashMap<>();
    setMarkers();

    // autonBuilder =
    //     new SwerveAutoBuilder(
    //         RobotContainer.swerve::getPose,
    //         RobotContainer.swerve::resetOdometry,
    //         new PIDConstants(Constants.AutonConstants.XY_CONTROLLER_P, 0.0, 0.0),
    //         new PIDConstants(Constants.AutonConstants.THETA_CONTROLLER_P, 0.0, 0.0),
    //         RobotContainer.swerve::setChassisSpeeds,
    //         eventMap,
    //         true,
    //         RobotContainer.swerve);

    autonChooser.setDefaultOption("No-op", new InstantCommand());
    // autonChooser.addOption("Testing", testing());
    // autonChooser.addOption("2 Cube No-Cable No-Balance", twoCubeNoCableRush());
    // autonChooser.addOption("3 Cube No-Cable No-Balance", threeCubeNoCableRush());
    SmartDashboard.putData("Auton Chooser", autonChooser);
  }

  private static void setMarkers() {
    eventMap.put("Wait a Second", new WaitCommand(1)); //Example marker

    eventMap.put("Stop Intake", new IntakeSpin(RobotContainer.intake, 0));
    eventMap.put("Intake", new IntakeCube(RobotContainer.intake, RobotContainer.arm, Constants.ArmConstants.INTAKE_POSITION));
    eventMap.put("Outtake", new OuttakeCube(RobotContainer.intake));
    eventMap.put("Set Max Outtake", new PrepareOuttakeSpeed(RobotContainer.intake, Constants.IntakeConstants.Intake_Speed.SHOOT_MAX_SPEED));
    eventMap.put("Set High Outtake", new PrepareOuttakeSpeed(RobotContainer.intake, Constants.IntakeConstants.Intake_Speed.SHOOT_HIGH_CUBE));
    eventMap.put("Set Mid Outtake", new PrepareOuttakeSpeed(RobotContainer.intake, Constants.IntakeConstants.Intake_Speed.SHOOT_MID_CUBE));  
    eventMap.put("Set Low Outtake", new PrepareOuttakeSpeed(RobotContainer.intake, Constants.IntakeConstants.Intake_Speed.SHOOT_LOW_CUBE)); 

    eventMap.put("Move Arm Rest", new MoveArmToPosition(RobotContainer.arm, Constants.ArmConstants.REST_POSITION));
    eventMap.put("Move Arm Mid", new MoveArmToPosition(RobotContainer.arm, Constants.ArmConstants.MID_POSITION));
    eventMap.put("Move Arm Low", new MoveArmToPosition(RobotContainer.arm, Constants.ArmConstants.LOW_POSITION));

    // eventMap.put("Lock Wheels", new InstantCommand(() -> RobotContainer.swerve.lock()));
    // eventMap.put("Set Gyro 180", new InstantCommand(() -> RobotContainer.swerve.offsetGyroYaw(180)));
    // eventMap.put("Set Heading 180", new InstantCommand(() -> RobotContainer.swerve.setHeadingAngle(180)));
    // eventMap.put("Zero Gyro", new InstantCommand(() -> RobotContainer.swerve.zeroGyro()));

    // eventMap.put("Balance", new Balance(RobotContainer.swerve)); Just send it or something lmao
  }

  public static Command getAutonomousCommand() {
    return autonChooser.getSelected();
  }

  // public static Command testing() {
  //   return autonBuilder.fullAuto(
  //       PathPlanner.loadPathGroup(
  //           "Testing", Constants.AutonConstants.CONSTRAINTS));
  // }
  // public static Command twoCubeNoCableBalance() {
  //   return autonBuilder.fullAuto(
  //       PathPlanner.loadPathGroup(
  //           "2 Cube No-Cable Balance", Constants.AutonConstants.CONSTRAINTS));
  // }

  // public static Command twoCubeCableRush() {
  //   return autonBuilder.fullAuto(
  //       PathPlanner.loadPathGroup(
  //           "2 Cube Rush Cable", Constants.AutonConstants.CONSTRAINTS));
  // }

  // public static Command twoCubeNoCableRush() {
  //   return autonBuilder.fullAuto(
  //       PathPlanner.loadPathGroup(
  //           "2 Cube Rush No-Cable", Constants.AutonConstants.CONSTRAINTS));
  // }

  // public static Command threeCubeNoCableBalance() {
  //   return autonBuilder.fullAuto(
  //       PathPlanner.loadPathGroup(
  //           "3 Cube No-Cable Balance", Constants.AutonConstants.CONSTRAINTS));
  // }

  // public static Command threeCubeCableRush() {
  //   return autonBuilder.fullAuto(
  //       PathPlanner.loadPathGroup(
  //           "3 Cube Rush Cable", Constants.AutonConstants.CONSTRAINTS));
  // }

  // public static Command threeCubeNoCableRush() {
  //   return autonBuilder.fullAuto(
  //       PathPlanner.loadPathGroup(
  //           "3 Cube Rush No-Cable", Constants.AutonConstants.CONSTRAINTS));
  // }

  // public static Command balance() {
  //   return autonBuilder.fullAuto(
  //       PathPlanner.loadPathGroup(
  //           "TestBalance", new PathConstraints(1.5, 3)));
  // }
}
