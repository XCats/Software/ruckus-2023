package frc.team191.commands.leds;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.team191.Constants;
import frc.team191.commands.arm.MoveArmToPosition;
import frc.team191.commands.intake.IntakeSpin;
import frc.team191.commands.intake.WaitForIntake;
import frc.team191.subsystems.LEDSubsystem;
import frc.team191.subsystems.LEDSubsystem.Section;
import frc.team191.subsystems.IntakeSubsystem;
import frc.team191.Constants.IntakeConstants.Intake_Speed;

public class SetLEDPattern extends InstantCommand{
    IntakeSubsystem intake;
    LEDSubsystem leds;

    public SetLEDPattern(IntakeSubsystem intakeSubsystem, LEDSubsystem ledSubsystem)
    {
        intake = intakeSubsystem;
        leds = ledSubsystem;
        addRequirements(leds);
    }

    @Override
    public void initialize () 
    {
       if(intake.isHoldingCube())
       {
            switch(intake.getOuttakeVelocity())
            {
                case CUBE_INTAKE_SPEED:
                    leds.setStrobe(Section.FULL, Color.kRed, Constants.LEDConstants.STROBE_FAST_DURATION); //Not Good Shouldnt Happen lmao
                    break;
                case SHOOT_MAX_SPEED:
                    leds.setRainbow(Section.FULL);
                    break;
                case SHOOT_HIGH_CUBE:
                    leds.setRuckusHold(Section.FULL, Color.kYellow, 7);
                    break;
                case SHOOT_MID_CUBE:
                    leds.setRuckusHold(Section.FULL, Color.kRed, 4);
                    break;
                case SHOOT_LOW_CUBE:
                    leds.setRuckusHold(Section.FULL, Color.kGreen, 1);
                    break;
                default:
                    leds.setStrobe(Section.FULL, Color.kBrown, Constants.LEDConstants.STROBE_SLOW_DURATION); //Not Good Shouldnt Happen lmao
                    break;
            }
       }
       else
       {
        leds.setBreath(Section.FULL, Color.kPurple, Color.kBlack, Constants.LEDConstants.BREATH_DURATION);
       }
    }
}

