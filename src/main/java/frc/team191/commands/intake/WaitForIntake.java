package frc.team191.commands.intake;

import edu.wpi.first.wpilibj2.command.Command;
import frc.team191.subsystems.IntakeSubsystem;

public class WaitForIntake extends Command {
    
    private IntakeSubsystem intake;
    
    public WaitForIntake(IntakeSubsystem intakeSubsystem){
        
        intake = intakeSubsystem;
    }

    @Override
    public void initialize() {
    }
  
    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {

    }
  
    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
    }
  
    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
      return intake.isHoldingCube();
    }

}
