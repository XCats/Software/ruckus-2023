package frc.team191.commands.intake;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.team191.subsystems.IntakeSubsystem;


public class IntakeSpin extends InstantCommand {
    
    IntakeSubsystem intake;
    double velocity;
    public IntakeSpin(IntakeSubsystem intakeSubsystem, double velocity) {
        intake = intakeSubsystem;
        this.velocity = velocity;
        addRequirements(intakeSubsystem);
    }

    @Override
    public void initialize () {
        intake.setVelocity(velocity);
    }
}