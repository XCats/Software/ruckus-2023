// package frc.team191.commands.intake;
// import edu.wpi.first.wpilibj2.command.ConditionalCommand;
// import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
// import edu.wpi.first.wpilibj2.command.WaitCommand;
// import frc.team191.subsystems.IntakeSubsystem;


// public class AcquireCube extends ConditionalCommand {

//     public AcquireCube(IntakeSubsystem intakeSubsystem){
//         super(new IntakeSpin(intakeSubsystem,0),
//             new SequentialCommandGroup(new IntakeSpin(intakeSubsystem, Constants.IntakeConstants.CUBE_INTAKE_SPEED), 
//                 new WaitUntilClawBumpSwitchIsPressed(clawSubsystem),
//                 new WaitCommand(0.5),
//                 new IntakeSpin(intakeSubsystem,0)),
//                 intakeSubsystem::isHoldingCube);
//     }
// }
