package frc.team191.commands.intake;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.team191.subsystems.IntakeSubsystem;
import frc.team191.Constants.IntakeConstants.Intake_Speed;


public class PrepareOuttakeSpeed extends InstantCommand {
    
    IntakeSubsystem intakeSubsystem;
    Intake_Speed velocity;
    public PrepareOuttakeSpeed(IntakeSubsystem intakeSubsystem, Intake_Speed velocity) {
        this.intakeSubsystem = intakeSubsystem;
        this.velocity = velocity;
        addRequirements(intakeSubsystem);
    }

    @Override
    public void initialize () {
        intakeSubsystem.setOuttakeVelocity(velocity);
    }
}