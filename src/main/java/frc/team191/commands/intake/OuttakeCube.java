package frc.team191.commands.intake;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import frc.team191.subsystems.IntakeSubsystem;


public class OuttakeCube extends InstantCommand {
    
    IntakeSubsystem intake;
    public OuttakeCube(IntakeSubsystem intakeSubsystem) {
        intake = intakeSubsystem;
        addRequirements(intakeSubsystem);
    }

    @Override
    public void initialize () {
        intake.shootCube();
    }
}