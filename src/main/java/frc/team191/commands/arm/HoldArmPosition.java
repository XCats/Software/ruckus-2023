package frc.team191.commands.arm;

import edu.wpi.first.wpilibj2.command.Command;
import frc.team191.subsystems.ArmSubsystem;

public class HoldArmPosition extends Command {
    
    final ArmSubsystem arm;

    public HoldArmPosition(ArmSubsystem arm)
  {
    this.arm = arm;
    addRequirements(arm);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize()
  {
    arm.setArmAngle(arm.getHoldArmAngle());
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute()
  {
  
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted)
  {
    // System.out.println("end");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished()
  {
    return Math.abs(arm.getArmAngle() - arm.getHoldArmAngle()) < 2;
  }
}