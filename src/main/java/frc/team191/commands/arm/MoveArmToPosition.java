package frc.team191.commands.arm;

import edu.wpi.first.wpilibj2.command.Command;
import frc.team191.subsystems.ArmSubsystem;

public class MoveArmToPosition extends Command {
    
    final ArmSubsystem arm;
    final Double angle;

    public MoveArmToPosition(ArmSubsystem arm, double angle)
  {
    this.arm = arm;
    this.angle = angle;
    addRequirements(arm);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize()
  {
    arm.setArmAngle(angle);
    arm.setHoldArmAngle(angle);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute()
  {
  
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted)
  {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished()
  {
    return Math.abs(arm.getArmAngle() - angle) < 2;
  }
}