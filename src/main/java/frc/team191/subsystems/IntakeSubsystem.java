package frc.team191.subsystems;

import frc.team191.Constants;

import com.revrobotics.SparkMaxPIDController;
import com.revrobotics.CANSparkBase.ControlType;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;
import frc.team191.Constants.IntakeConstants.Intake_Speed;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class IntakeSubsystem extends SubsystemBase
{
    private CANSparkMax intakeMotor = new CANSparkMax(Constants.IntakeConstants.INTAKE_MOTOR_TOP, MotorType.kBrushless);
    private CANSparkMax intakeMotorFollower = new CANSparkMax(Constants.IntakeConstants.INTAKE_MOTOR_BOT, MotorType.kBrushless);

    private final DigitalInput beamBreak = new DigitalInput(Constants.IntakeConstants.BEAM_BREAK_DIO);

    private Intake_Speed outtakeVelocity = Constants.IntakeConstants.Intake_Speed.SHOOT_MAX_SPEED;

    public IntakeSubsystem()
    {
        setupMotors();
        if(Constants.GeneralConstants.IS_TUNING_MODE)
        {
            SmartDashboard.putNumber("Outtake Tune Value", outtakeVelocity.getValue());
        }
    }

    public void setVelocity(double velocity)
    {
        intakeMotor.set(velocity);
        intakeMotorFollower.set(velocity);
    }

    public void setOuttakeVelocity(Intake_Speed velocity)
    {
        outtakeVelocity = velocity;
    }

    public Intake_Speed getOuttakeVelocity()
    {
        return outtakeVelocity;
    }

    public void shootCube()
    {
        intakeMotor.set(outtakeVelocity.getValue());
        intakeMotorFollower.set(outtakeVelocity.getValue());
    }

    public boolean isHoldingCube()
    {
        return !beamBreak.get();
    }

    public void setupMotors()
    {
        
        intakeMotorFollower.restoreFactoryDefaults();
        intakeMotor.restoreFactoryDefaults();

        intakeMotor.setIdleMode(IdleMode.kCoast);
        intakeMotorFollower.setIdleMode(IdleMode.kCoast);

        // intakeMotor.follow(intakeMotor, false);

        intakeMotor.setInverted(false);
        intakeMotorFollower.setInverted(false);

        intakeMotor.setSmartCurrentLimit(Constants.IntakeConstants.CURRENT_LIMIT_INTAKE);
        intakeMotorFollower.setSmartCurrentLimit(Constants.IntakeConstants.CURRENT_LIMIT_INTAKE);

        // intakePID = intakeMotor.getPIDController();

        // intakePID.setP(Constants.IntakeConstants.INTAKE_PID0_P, 0);
        // intakePID.setI(Constants.IntakeConstants.INTAKE_PID0_I, 0);
        // intakePID.setD(Constants.IntakeConstants.INTAKE_PID0_D, 0);
        // intakePID.setIZone(0, 0);
        // intakePID.setFF(Constants.IntakeConstants.INTAKE_PID0_F, 0);
        // intakePID.setOutputRange(Constants.IntakeConstants.OUTPUT_RANGE_MIN, Constants.IntakeConstants.OUTPUT_RANGE_MAX, 0);

        // intakePID.setP(Constants.IntakeConstants.INTAKE_PID1_P, 1);
        // intakePID.setI(Constants.IntakeConstants.INTAKE_PID1_I, 1);
        // intakePID.setD(Constants.IntakeConstants.INTAKE_PID1_D, 1);
        // intakePID.setIZone(0, 1);
        // intakePID.setFF(Constants.IntakeConstants.INTAKE_PID1_F, 1);
        // intakePID.setOutputRange(Constants.IntakeConstants.OUTPUT_RANGE_MIN, Constants.IntakeConstants.OUTPUT_RANGE_MAX, 1);

        intakeMotor.setOpenLoopRampRate(0.05);
        
        intakeMotor.burnFlash();
        intakeMotorFollower.burnFlash();
    }
    public void periodic(){
        SmartDashboard.putBoolean("Holding Cube", isHoldingCube());
        SmartDashboard.putNumber("Outtake Value", outtakeVelocity.getValue());
    }
}
