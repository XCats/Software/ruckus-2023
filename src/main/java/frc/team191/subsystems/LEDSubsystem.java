package frc.team191.subsystems;

import java.util.List;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.team191.Constants;

public class LEDSubsystem extends SubsystemBase {
    private final AddressableLED leds = new AddressableLED(Constants.LEDConstants.LED_PORT);
    private final AddressableLEDBuffer buffer = new AddressableLEDBuffer(Constants.LEDConstants.FULL_LENGTH);

    public LEDSubsystem()
    {
        leds.setLength(buffer.getLength());
        leds.setData(buffer);
        leds.start();
    }

    public void periodic()
    {
        // setSolid(Section.FULL, Color.kRed);
        leds.setData(buffer);
    }

    public void setSolid(Section section, Color color)
    {
        if (color == null)
        {
            // switch(DriverStation.getAlliance())
            // {
            //     case Red:
            //         color = Color.kRed;
            //     case Blue:
            //         color = Color.kBlue;
            //     default:
                    color = Color.kPurple;
            // }
        }
        for(int i = section.start(); i < section.end(); i++)
        {
            buffer.setLED(i, color);
        }
    }

    public void setStrobe(Section section, Color color, double duration)
    {
        boolean on = ((Timer.getFPGATimestamp() % duration) / duration) > 0.5;
        setSolid(section, on ? color : Color.kBlack);
    }

    public void setBreath(Section section, Color c1, Color c2, double duration)
    {
        setBreath(section, c1, c2, duration, Timer.getFPGATimestamp());
    }

    public void setBreath(Section section, Color c1, Color c2, double duration, double timestamp)
    {
        double x = ((timestamp % Constants.LEDConstants.BREATH_DURATION) / Constants.LEDConstants.BREATH_DURATION) * 2.0 * Math.PI;
        double ratio = (Math.sin(x) + 1.0) / 2.0;
        double red = (c1.red * (1 - ratio)) + (c2.red * ratio);
        double green = (c1.green * (1 - ratio)) + (c2.green * ratio);
        double blue = (c1.blue * (1 - ratio)) + (c2.blue * ratio);
        setSolid(section, new Color(red, green, blue));
    }

    public void setWave(Section section, Color c1, Color c2, double cycleLength, double duration) {
        double x = (1 - ((Timer.getFPGATimestamp() % duration) / duration)) * 2.0 * Math.PI;
        double xDiffPerLed = (2.0 * Math.PI) / cycleLength;
        for (int i = 0; i < section.end(); i++) {
          x += xDiffPerLed;
          if (i >= section.start()) {
            double ratio = (Math.pow(Math.sin(x), Constants.LEDConstants.WAVE_EXPONENT) + 1.0) / 2.0;
            if (Double.isNaN(ratio)) {
              ratio = (-Math.pow(Math.sin(x + Math.PI), Constants.LEDConstants.WAVE_EXPONENT) + 1.0) / 2.0;
            }
            if (Double.isNaN(ratio)) {
              ratio = 0.5;
            }
            double red = (c1.red * (1 - ratio)) + (c2.red * ratio);
            double green = (c1.green * (1 - ratio)) + (c2.green * ratio);
            double blue = (c1.blue * (1 - ratio)) + (c2.blue * ratio);
            buffer.setLED(i, new Color(red, green, blue));
          }
        }
      }
    
      public void setStripes(Section section, List<Color> colors, int length, double duration) {
        int offset = (int) (Timer.getFPGATimestamp() % duration / duration * length * colors.size());
        for (int i = section.start(); i < section.end(); i++) {
          int colorIndex =
              (int) (Math.floor((double) (i - offset) / length) + colors.size()) % colors.size();
          colorIndex = colors.size() - 1 - colorIndex;
          buffer.setLED(i, colors.get(colorIndex));
        }
      }

    public void setRainbow(Section section)
    {
        double x = (1 - ((Timer.getFPGATimestamp() / Constants.LEDConstants.RAINBOW_DURATION) % 1.0)) * 180.0;
        double xDiffPerLed = 180.0 / Constants.LEDConstants.RAINBOW_CYCLE_LENGTH;
        for (int i = 0; i < section.end(); i++) 
        {
            x += xDiffPerLed;
            x %= 180.0;
            if (i >= section.start()) 
            {
                buffer.setHSV(i, (int) x, 255, 255);
            }
        }
    }

    public void setRuckusHold(Section section, Color color, int offset)
    {
      setRuckusHold(section, color, offset, Timer.getFPGATimestamp());
    }

    public void setRuckusHold(Section section, Color color, int offset, double timestamp)
    {
        double x = ((Timer.getFPGATimestamp() % Constants.LEDConstants.BREATH_DURATION) / Constants.LEDConstants.BREATH_DURATION) * 2.0 * Math.PI;
        double ratio = (Math.sin(x) + 1.0) / 2.0;
        double red = (color.red * (1 - ratio)) + (Color.kBlack.red * ratio);
        double green = (color.green * (1 - ratio)) + (Color.kBlack.green * ratio);
        double blue = (color.blue * (1 - ratio)) + (Color.kBlack.blue * ratio);
        color = new Color(red, green, blue);
        for (int i = section.start(); i < section.end(); i++) 
        {
            if(((i%15) >= (7-offset)) && ((i%15) <=(7+offset)))
            {
              buffer.setLED(i, color);
            }
            else
            {
              buffer.setLED(i,Color.kBlack);
            }

        }
    }

    public static enum Section {
        CHASSIS,
        INTAKE,
        FULL;
    
        private int start() {
          switch (this) {
            case CHASSIS:
              return 0;
            case INTAKE:
              return Constants.LEDConstants.CHASSIS_LENGTH;
            case FULL:
              return 0;
            default:
              return 0;
          }
        }
    
        private int end() {
          switch (this) {
            case CHASSIS:
              return Constants.LEDConstants.CHASSIS_LENGTH;
            case INTAKE:
              return Constants.LEDConstants.FULL_LENGTH - Constants.LEDConstants.CHASSIS_LENGTH;
            case FULL:
              return Constants.LEDConstants.FULL_LENGTH;
            default:
              return Constants.LEDConstants.FULL_LENGTH;
          }
        }
      }

}
