package frc.team191.subsystems;
import com.revrobotics.SparkMaxPIDController;
import com.revrobotics.CANSparkBase.ControlType;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.AbsoluteEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.SparkMaxAbsoluteEncoder.Type;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.team191.Constants;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class ArmSubsystem extends SubsystemBase{

    private CANSparkMax armMotor = new CANSparkMax(Constants.ArmConstants.ARM_MOTOR, MotorType.kBrushless);
    private CANSparkMax armMotorFollower = new CANSparkMax(Constants.ArmConstants.ARM_MOTOR_FOLLOWER, MotorType.kBrushless);

    private AbsoluteEncoder armEncoder;

    private SparkMaxPIDController armPID;

    private double holdArmAngle;

    public ArmSubsystem() 
    {
        setupMotors();
        if(Constants.GeneralConstants.IS_TUNING_MODE)
        {
          // SmartDashboard.putNumber("Arm Tune Value", getArmAngle());
          // SmartDashboard.putNumber("Arm P Value", armPID.getP());
          // SmartDashboard.putNumber("Arm I Value", armPID.getI());
          // SmartDashboard.putNumber("Arm D Value", armPID.getD());
          // SmartDashboard.putNumber("Arm F Value", armPID.getFF());
        }
        // holdArmAngle = getArmAngle();
        holdArmAngle = Constants.ArmConstants.REST_POSITION;
        // SmartDashboard.putNumber("Last Arm Angle", getHoldArmAngle());

    }

    public double getArmAngle() 
    {
      return armEncoder.getPosition();
    }

    public double getHoldArmAngle()
    {
      return holdArmAngle;
    }

    public void setHoldArmAngle(double angle)
    {
      holdArmAngle = angle;
    }
    
    public void setArmAngle(double angle) 
    {
      armPID.setReference(angle, ControlType.kPosition);
    }

    public void setupMotors()
    {

        armMotor.restoreFactoryDefaults();
        armMotorFollower.restoreFactoryDefaults();

        armMotor.setInverted(false);
        armMotorFollower.follow(armMotor,true);

        armMotor.setIdleMode(IdleMode.kBrake);
        armMotorFollower.setIdleMode(IdleMode.kBrake);

        armMotor.setSmartCurrentLimit(Constants.ArmConstants.CURRENT_LIMIT_ARM);
        armMotorFollower.setSmartCurrentLimit(Constants.ArmConstants.CURRENT_LIMIT_ARM);

        armEncoder = armMotor.getAbsoluteEncoder(Type.kDutyCycle);
        armEncoder.setInverted(true);
        armEncoder.setPositionConversionFactor(360);//Degrees per rotation
        armEncoder.setZeroOffset(180);
  
        armPID = armMotor.getPIDController();
        armPID.setPositionPIDWrappingEnabled(false);

        armPID.setFeedbackDevice(armEncoder);

        armPID.setP(Constants.ArmConstants.ARM_PID0_P, 0);
        armPID.setI(Constants.ArmConstants.ARM_PID0_I, 0);
        armPID.setD(Constants.ArmConstants.ARM_PID0_D, 0);
        armPID.setIZone(0, 0);
        armPID.setFF(Constants.ArmConstants.ARM_PID0_F, 0);
        armPID.setOutputRange(Constants.ArmConstants.OUTPUT_RANGE_MIN, Constants.ArmConstants.OUTPUT_RANGE_MAX, 0);
          
        armMotor.burnFlash();
        armMotorFollower.burnFlash();
    }

    public void periodic(){
      // SmartDashboard.putNumber("Arm Angle", getArmAngle());
      // SmartDashboard.putNumber("Last Arm Angle", getHoldArmAngle());

      if(Constants.GeneralConstants.IS_TUNING_MODE)
      {
        // double armVal = SmartDashboard.getNumber("Arm Tune Value", getArmAngle());
        // setArmAngle(armVal);
        // armPID.setP(SmartDashboard.getNumber("Arm P Value", armPID.getP()));
        // armPID.setI(SmartDashboard.getNumber("Arm I Value", armPID.getI()));
        // armPID.setD(SmartDashboard.getNumber("Arm D Value", armPID.getD()));
        // armPID.setFF(SmartDashboard.getNumber("Arm F Value", armPID.getFF()));
      }
    }
    }
